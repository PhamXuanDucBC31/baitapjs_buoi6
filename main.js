/* bài 1: tìm số nguyên dương nhỏ nhất*/
document.getElementById("find_n").addEventListener("click", function () {
  var sum = 0;
  var n = 0;
  while (sum < 10000) {
    sum = sum + n;
    n = n + 1;
  }
  document.getElementById("result_find_n").innerHTML = `<div> N= ${
    n - 1
  } </div>`;
});
// bài 2: tính tổng số mũ
document.getElementById("sum_power").addEventListener("click", function () {
  let x = document.getElementById("num_X").value * 1;
  let n = document.getElementById("num_N").value * 1;
  var sum = 0;
  for (var i = 1; i <= n; i++) {
    sum = sum + Math.pow(x, i);
  }
  document.getElementById(
    "result_sum_power"
  ).innerHTML = `<div>Tổng: ${sum} </div>`;
});
// bài 3: tính giai thừa
document.getElementById("factorial_N").addEventListener("click", function () {
  let fac_n = document.getElementById("factorial_num_N").value * 1;
  factorialOfN = 1;
  for (var i = 1; i <= fac_n; i++) {
    factorialOfN = factorialOfN * i;
  }
  document.getElementById(
    "result_factorial"
  ).innerHTML = `<div> Giai thừa của ${fac_n} là : ${factorialOfN} </div>`;
});
// bài 4: tạo thẻ div
document.getElementById("div_create").addEventListener("click", function () {
  for (var i = 1; i <= 10; i++) {
    if (parseInt(i % 2) == 0) {
      var div = document.createElement("div");
      div.style.width = "100%";
      div.style.height = "40px";
      div.style.background = "red";
      div.style.marginBottom = "5px";
      div.style.color = "white";
      div.innerHTML = "Div chẵn";
      div.style.padding = "0px 10px";
      div.style.borderRadius = "10px";
      document.getElementById("create_div_tag").appendChild(div);
    } else {
      var div = document.createElement("div");
      div.style.width = "100%";
      div.style.height = "40px";
      div.style.background = "blue";
      div.style.marginBottom = "5px";
      div.style.color = "white";
      div.innerHTML = "Div lẻ";
      div.style.padding = "0px 10px";
      div.style.borderRadius = "10px";
      document.getElementById("create_div_tag").appendChild(div);
    }
  }
});
// bài 5: in số nguyên tố từ 1 tới N
function prime_test(x) {
  var S = 0;
  for (var i = 2; i < x; i++) {
    if (parseInt(x % i) == 0) {
      S = S + 1;
    }
  }
  return S;
}
document.getElementById("prime_N").addEventListener("click", function () {
  document.getElementById("result_prime").innerHTML = "";
  let prime_n = document.getElementById("prime_num_N").value * 1;
  for (var i = 1; i <= prime_n; i++) {
    if (prime_test(i) == 0) {
      var div = document.createElement("div");
      div.style.width = "40px";
      div.style.height = "40px";
      div.style.color = "#black";
      div.style.marginRight = "20px";
      div.innerHTML = i;
      div.style.fontSize = "25px";
      document.getElementById("result_prime").appendChild(div);
    }
  }
});
